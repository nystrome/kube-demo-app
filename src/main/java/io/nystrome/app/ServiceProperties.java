package io.nystrome.app;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties("app")
public class ServiceProperties {

    /**
     * Message used in the app
     */
    private String message;

}