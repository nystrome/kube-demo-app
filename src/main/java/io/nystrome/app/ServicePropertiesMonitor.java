package io.nystrome.app;

import static net.logstash.logback.argument.StructuredArguments.kv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ServicePropertiesMonitor {

    @Autowired
    private ServiceProperties serviceProperties;

    @Scheduled(cron = "${app.scheduling.cron:-}")
    public void servicePropertiesRadiator() {
        log.info("Items available in service properties", 
            kv("custom.service.message", serviceProperties.getMessage()));
    }

}
