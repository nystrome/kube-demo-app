package io.nystrome.app;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Slf4j
@RestController
public class DemoController {

	@Autowired
	DiscoveryClient discoveryClient;

	@Autowired
	private ServiceProperties serviceProperties;

	@GetMapping("/message")
	public String message() {
		return serviceProperties.getMessage();
	}

	@GetMapping("/combined-message")
	public String combinedMessage() {
		RestTemplate rt = new RestTemplate();
		String otherMessage = rt.getForObject("http://kube-demo-dep:8080/message", String.class);
		String myMessage = serviceProperties.getMessage();
		log.info("messages were found", kv("custom.service.message", myMessage),
			kv("custom.dependency.message", otherMessage));
		return myMessage + otherMessage;
	}

	@GetMapping("/services")
	public List<String> services() {
		return this.discoveryClient.getServices();
	}

	@GetMapping("/")
	public String index() {
		return "this is the index page";
	}
}
